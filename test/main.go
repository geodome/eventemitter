package main

import (
	"fmt"
	"time"

	"bitbucket.org/geodome/eventemitter"
)

func main() {
	a := eventemitter.New()
	a.On("World", func() {
		fmt.Println("World detected")
	})
	a.On("hello", func() {
		fmt.Println("hello detected")
	})
	a.On("hello", func(x int, y int) {
		fmt.Println("Asd", x+y)
		a.Emit("World")
	})
	a.Emit("hello", 11, 12)
	time.Sleep(2 * time.Second)
}
