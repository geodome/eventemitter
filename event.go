package eventemitter

import (
	"reflect"

	"github.com/google/uuid"
)

type handler struct {
	on  chan []reflect.Value
	off chan bool
}

//Event represents an event and contains all the corresponding event handlers
type Event struct {
	tag string
	on  map[ID]*handler
}

//NewEvent : This method creates a new Event
func NewEvent(tag string) *Event {
	return &Event{
		tag: tag,
		on:  make(map[ID]*handler),
	}
}

// On enrols a go routine for "on" event handlers
func (e *Event) On(fn interface{}) ID {
	if reflect.ValueOf(fn).Kind() == reflect.Func {
		id := uuid.New()
		e.on[id] = &handler{
			on:  make(chan []reflect.Value),
			off: make(chan bool),
		}
		go func() {
			nparam := reflect.TypeOf(fn).NumIn()
			for {
				select {
				case rargs := <-e.on[id].on:
					if len(rargs) > 0 {
						reflect.ValueOf(fn).Call(rargs[:nparam])
					}
				case toOff := <-e.on[id].off:
					if toOff {
						delete(e.on, id)
						return
					}
				}
			}
		}()
		return id
	}
	return uuid.Nil
}

// Once enrols a go routine for "once" event handlers
func (e *Event) Once(fn interface{}) ID {
	if reflect.ValueOf(fn).Kind() == reflect.Func {
		id := uuid.New()
		e.on[id] = &handler{
			on:  make(chan []reflect.Value),
			off: make(chan bool),
		}
		go func() {
			nparam := reflect.TypeOf(fn).NumIn()
			for {
				select {
				case rargs := <-e.on[id].on:
					if len(rargs) > 0 {
						reflect.ValueOf(fn).Call(rargs[:nparam])
						e.on[id].off <- true
					}
				case toOff := <-e.on[id].off:
					if toOff {
						delete(e.on, id)
						return
					}
				}
			}
		}()
		return id
	}
	return uuid.Nil
}

// Off switches off all the event handlers attached to this event
func (e *Event) Off() {
	for _, h := range e.on {
		h.off <- true
	}
}

// OffID switches off a specific handler
func (e *Event) OffID(id ID) {
	if _, ok := e.on[id]; ok {
		e.on[id].off <- true
	}
}

// Emit events
func (e *Event) Emit(rargs []reflect.Value) {
	for id := range e.on {
		e.on[id].on <- rargs
	}
}
