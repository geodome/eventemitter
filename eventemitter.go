package eventemitter

import (
	"reflect"

	"github.com/google/uuid"
)

// EventEmitter represents an EventEmitter object
type EventEmitter struct {
	events map[string]*Event
}

// ID is actually uuid.UUID type. By exporting ID, the underlying uuid.UUID doesn't have to be explicitly exported by the user.
type ID = uuid.UUID

// Emitter describes the interface
type Emitter interface {
	On(string, interface{}) ID
	Once(string, interface{}) ID
	Off(string)
	OffID(string, ID)
	Emit(string, ...interface{})
}

// New generates a new Event Emitter
func New() Emitter {
	return &EventEmitter{events: make(map[string]*Event)}
}

// On enrols a new event handler
func (ee *EventEmitter) On(tag string, fn interface{}) ID {
	if reflect.ValueOf(fn).Kind() == reflect.Func {
		if _, ok := ee.events[tag]; !ok {
			ee.events[tag] = NewEvent(tag)
		}
		return ee.events[tag].On(fn)
	}
	return uuid.Nil
}

// Once enrols a new event handler
func (ee *EventEmitter) Once(tag string, fn interface{}) ID {
	if reflect.ValueOf(fn).Kind() == reflect.Func {
		if _, ok := ee.events[tag]; !ok {
			ee.events[tag] = NewEvent(tag)
		}
		return ee.events[tag].Once(fn)
	}
	return uuid.Nil
}

// Off removes all event handlers for an event
func (ee *EventEmitter) Off(tag string) {
	if _, ok := ee.events[tag]; ok {
		ee.events[tag].Off()
		delete(ee.events, tag)
	}
}

// OffID removes the specific handler
func (ee *EventEmitter) OffID(tag string, id ID) {
	if _, ok := ee.events[tag]; ok {
		ee.events[tag].OffID(id)
	}
}

// Emit events
func (ee *EventEmitter) Emit(tag string, args ...interface{}) {
	if _, ok := ee.events[tag]; ok {
		rargs := make([]reflect.Value, len(args)+1)
		for i := 0; i < len(args); i++ {
			rargs[i] = reflect.ValueOf(args[i])
		}
		rargs[len(args)] = reflect.ValueOf(true)
		ee.events[tag].Emit(rargs)
	}
}
