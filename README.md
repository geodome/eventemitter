# Event Emitter

Simple Event Emitter for the Go Programming language. Inspired by [Node.js Event Emitter](https://nodejs.org/api/events.html)

Version 0.1

# People
The author of eventemitter is Donaldson Tan / [@geodome](https://www.bitbucket.org/geodome)

# Installation
The only requirement for the Go language
```bash
$ go get -u bitbucket.org/geodome/eventemitter
```
# Overview

## New()
- returns a new EventEmitter instance

## EventEmitter.On(eventName string, listener func) UUID
- Enrols a listener for the given event name
- Returns a unique ID value that represents the listener

## EventEmitter.Once(eventName string, handler func) UUID
- Enrols a listener for the given event name
- The listener will execute at most once.
- Returns a unique ID value that represents the listener

## EventEmitter.Off(eventName string)
- Terminates all listeners for the given event name

## EventEmitter.OffID(eventName string, id UUID)
- Terminates the handler with id for the given event name

## EventEmitter.Emit(eventName string, parameters ...interface{})
- Emits the given event name with the parameters

# Specification of Listener
- Each listener has a void return type
- Each listener can have multiple parameters.
- Error propagation is done through the "error" event.
- At the moment, there is no plan for panic recovery.

# Code Example
```go
import (
    "fmt"
    "time"
    "bitbucket.org/geodome/eventemitter"
)

func main() {
    // Declare new instance of Event Emitter
    ee := eventemitter.New()

    // Enrol a handler
    ee.On("event1", func() {
        fmt.Println("event1 was detected")
    })
    ee.Emit("event1")
    // output: event1 was detected

    ee.On("event2", func(a int, b int) {
        fmt.Println("event2: sum is", a+b)
    })
    ee.Emit("event2", 1, 2)
    //output: event2: sum is 3

    ee.Once("event3", func() {
        fmt.Println("event3 was detected")
    })
    ee.Emit("event3")
    // output: event3 was detected
    ee.Emit("event3")
    // No output

    ee.Emit("event4", func() {
        fmt.Println("event4 was detected")
    })
    ee.Emit("event4")
    // output: event4 was detected
    ee.Off("event4")
    ee.Emit("event4")
    // no output

    id1 := ee.On("event5", func() {
        fmt.Println("Listener 1 responds to event5")
    })
    id2 := ee.On("event5", func() {
        fmt.Println("Listener 2 responds to event5")
    })
    ee.Emit("event5")
    //output:
    // Listener 1 responds to event5
    // Listener 2 responds to event5
    ee.Off("event5", id2)
    ee.Emit("event5")
    // output: Listener 1 responds to event5

    time.Sleep(5*time.Second)
}
```

# Future features
* Panic recovery for Listener. Needs a mechanism to convert panic to error.
* Error handling is very un-Go. Need to do something about it.

# License
This project is licensed under the MIT License.

License can be found [here](https://www.bitbucket.org/shukra-networks/eventemitter/LICENSE)